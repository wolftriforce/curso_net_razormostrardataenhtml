﻿using Curso_NET_RazorMostrarDataEnHTML.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Curso_NET_RazorMostrarDataEnHTML.Services
{
    public class PeliculasService
    {
        public Pelicula ObtenerPelicula()
        {
            return new Pelicula()
            {
                Titulo = "Monsers INC",
                Pais = "USA",
                Duracion = 115,
                Publicacion = new DateTime(2013, 12, 5)
            };
        }

        public List<Pelicula> ObtenerPeliculas()
        {
            var pelicula1 = new Pelicula()
            {
                Titulo = "Toy Story",
                Duracion = 100,
                Pais = "USA",
                Publicacion = new DateTime(2001, 12, 5)
            };

            var pelicula2 = new Pelicula()
            {
                Titulo = "Buscando a Nemo",
                Duracion = 200,
                Pais = "USA",
                Publicacion = new DateTime(2002, 10, 5)
            };

            var pelicula3 = new Pelicula()
            {
                Titulo = "La Sirenita",
                Duracion = 130,
                Pais = "USA",
                Publicacion = new DateTime(1995, 10, 5)
            };

            return new List<Pelicula> { pelicula1, pelicula2, pelicula3 };
        }


    }
}